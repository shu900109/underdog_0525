using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonaldJumpCtrl : MonoBehaviour
{
    public GameObject jumpAlart;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "JumpAttack")
        {
            jumpAlart.SetActive(false);
            Destroy(gameObject, 0.5f);
        }
    }
}
