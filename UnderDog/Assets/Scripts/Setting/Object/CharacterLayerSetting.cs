using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLayerSetting : MonoBehaviour
{
    public float groundPos;
    float ground_y;
    void Update()
    {
        ground_y = gameObject.transform.position.y + groundPos;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, transform.position.y, ground_y / 50);
    }
}
